/*
	 - youtube iframe docs
	 https://developers.google.com/youtube/iframe_api_reference */

/* a track in the sampler */
customElements.define(
	"sampler-track",
	class extends HTMLElement {
		static observedAttributes() {
			return ["src", "title"];
		}
		get src() {
			return this.getAttribute("src");
		}
		get id() {
			/* https://regexr.com/3a2p0 */
			const youtubeRegex =
				/(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\?(?:\S*?&?v\=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})/g;
			return this.getAttribute("id") || youtubeRegex.exec(this.src)[1];
		}
		get title() {
			return this.getAttribute("title");
		}
		get start() {
			return Number(this.getAttribute("start"));
		}
		get end() {
			return Number(this.getAttribute("end"));
		}
		get track() {
			return {
				src: this.src,
				title: this.title,
				start: this.start,
				end: this.end,
				id: this.id,
			};
		}
		connectedCallback() {
			this.innerText = this.title;
			this.addEventListener("click", this.handleClick.bind(this));
		}
		handleClick(event) {
			console.log("ev", this.track);
			const clickEvent = new CustomEvent("play", {
				bubbles: true,
				detail: {
					track: this.track,
				},
			});
			this.dispatchEvent(clickEvent);
		}
	},
);

/* the sample that has tracks */
const templateSampler = document.createElement("template");
templateSampler.innerHTML = `
	<slot name="data"></slot>
	<slot name="player">
		<div id="player">
			Loading player...
		</div>
	</slot>
	<slot name='list'>
		Loading samples track list...
	</slot>
	<style>
		:host {
			--size: 1.5rem;
			--c-bg: white;
			--c-bg--active: turquoise;
		}
		:host {
			display: flex;
			flex-direction: column;
		}
		slot[name="player"] {
			display: flex;
			position: sticky;
			top: 0;
		}
		slot[name="player"] iframe {
			height: 100%;
			max-height: 30vh;
		}
		slot[name="list"] {
			display: flex;
			flex-direction: column;
		}
		slot[name="data"] {
			display: flex;
			padding: calc(var(--size) / 2);
			/* flex-wrap: wrap; */
		}
		slot[name="data"] a {
			padding: calc(var(--size) / 3);
		}
		slot[name="data"] input {
			width: 100%;
		}
		sampler-track {
			margin-bottom: var(--size);
			padding: calc(var(--size) / 2);
			cursor: pointer;
		}
		sampler-track:hover {
			background-color: var(--c-bg--active);
		}

	</style>
`;
customElements.define(
	"web-sampler",
	class extends HTMLElement {
		playerConfig = {
			width: "100%",
			height: "400px",
			videoId: "7nQ2oiVqKHw",
			playerVars: {
				playsinline: 1,
			},
			events: {
				onReady: this.onPlayerReady.bind(this),
				onStateChange: this.onPlayerStateChange.bind(this),
			},
		};
		get gSheetUrl() {
			/* https://docs.google.com/spreadsheets/d/1SPHghYOD0zk1NpdRk8gk75ZhHThvE1FjQvH7IXqFLT8/edit?usp=sharing */
			return this.getAttribute("google-sheet-url");
		}
		get gSheetCsvUrl() {
			/* https://docs.google.com/spreadsheets/d/e/2PACX-1vSw-AEViAF1qEmVE5wYLjYg7-4IBLBNWJx2dY3HTdLLhg43NP6kALVeGILtooeXMGMYsv5I6N3-cwak/pub?output=csv */
			try {
				const url = new URL("pub", this.gSheetUrl);
				url.searchParams.set("output", "csv");
				return url.href;
			} catch (e) {
				// do nothing
			}
		}
		constructor() {
			super();
			this.attachShadow({ mode: "open" });
			this.shadowRoot.appendChild(templateSampler.content.cloneNode(true));
			this.$data = this.shadowRoot.querySelector('slot[name="data"]');
			this.$list = this.shadowRoot.querySelector('slot[name="list"]');
			this.$player = this.shadowRoot.querySelector('slot[name="player"]');
		}
		async attributeChangedCallback(name) {
			if (name === "google-sheet-url") {
				await this.fetchSheet();
				this.renderList();
			}
		}
		async connectedCallback() {
			this.renderYtScript();
			const $tracks = [...this.querySelectorAll("sampler-track")];
			const tracks = $tracks.map(($track) => {
				return $track.track;
			});

			if (this.gSheetCsvUrl) {
				await this.fetchSheet();
			}
			this.renderHeader();
			this.renderList();
		}
		async fetchSheet() {
			if (this.gSheetCsvUrl) {
				const res = await fetch(this.gSheetCsvUrl);
				const csvData = await res.text();
				const jsonData = this.csvJSON(csvData);
				this.tracks = jsonData;
			}
		}
		renderHeader() {
			const $database = document.createElement("a");
			$database.setAttribute("href", this.gSheetUrl);
			$database.innerText = "database";

			const $databaseInput = document.createElement("input");
			$databaseInput.value = this.gSheetUrl;
			$databaseInput.setAttribute("type", "text");
			$databaseInput.setAttribute(
				"placeholder",
				"Google Spreadsheet Public Sharing URL (or public CSV export URL)",
			);
			$databaseInput.addEventListener("input", (event) => {
				this.setAttribute("google-sheet-url", event.target.value);
			});

			this.$data.append($database);
			this.$data.append($databaseInput);
		}
		renderList() {
			this.$list.innerHTML = "";
			if (this.tracks) {
				/* tracks */
				this.tracks.forEach((track) => {
					const $track = document.createElement("sampler-track");
					$track.addEventListener("play", this.onTrackPlay.bind(this));
					$track.setAttribute("title", track["title"]);
					$track.setAttribute("src", track["url"]);
					this.$list.append($track);
				});
			} else {
				const $error = document.createElement("p");
				$error.innerText = "The data source has an error or no tracks";
				this.$list.append($error);
			}
		}
		onYouTubeIframeAPIReady() {
			console.log("youtube iframe ready: yt", YT);
			this.setAttribute("is-ready", true);
			this.player = new YT.Player(
				this.shadowRoot.getElementById("player"),
				this.playerConfig,
			);
		}
		onPlayerStateChange() {
			/* console.log('player state change') */
		}
		onPlayerReady() {
			/* console.log('player ready') */
		}
		renderYtScript() {
			const ytScriptTag = document.createElement("script");
			ytScriptTag.src = "https://www.youtube.com/iframe_api";
			this.$player.append(ytScriptTag);

			/* create an <iframe> (and YouTube player) after the API code downloads. */
			globalThis.onYouTubeIframeAPIReady =
				this.onYouTubeIframeAPIReady.bind(this);
		}
		onTrackPlay({ detail }) {
			console.log("detail", detail);
			this.player.loadVideoById({
				videoId: detail.track.id,
				startSeconds: detail.track.start,
				endSeconds: detail.track.end,
			});
		}

		//var csv is the CSV file with headers
		csvJSON(csv) {
			const cleanCell = (header) => {
				const $el = document.createElement("div");
				$el.innerText = header;
				return $el.innerText;
			};

			var lines = csv.split("\n");
			var result = [];
			var headers = lines[0].split(",").map(cleanCell);
			for (var i = 1; i < lines.length; i++) {
				var obj = {};
				var currentline = lines[i].split(",");
				for (var j = 0; j < headers.length; j++) {
					obj[headers[j]] = cleanCell(currentline[j]);
				}
				result.push(obj);
			}
			return result;
		}
	},
);
